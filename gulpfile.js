//init plugins
var lr = require('tiny-lr'), // web server livereload
    gulp = require('gulp'),
    jade = require('gulp-jade'),
    stylus = require('gulp-stylus'),
    livereload = require('gulp-livereload'),
    myth = require('gulp-myth'), // http://www.myth.io/
    csso = require('gulp-csso'), // CSS min
    imagemin = require('gulp-imagemin'),
    uglify = require('gulp-uglify'), // JS min
    concat = require('gulp-concat'),
    connect = require('connect'), // Webserver
    ngmin = require('gulp-ngmin');

var server = lr();

// https://www.npmjs.com/package/gulp-awspublish
var awspublish = require('gulp-awspublish');

var gulpNgConfig = require('gulp-ng-config');

var bump = require('gulp-bump');
var argv = require('yargs').argv; //for reading gulp task parameters

var gulpDocs = require('gulp-ngdocs');

var pkg = require('./package.json'); //package.json object
var config = pkg.config;


// settings
var PROJECT_FILES = './app';
var DEV_ENV = './dev';
var DEV_ENV_PORT = 9000;
var DEV_ENV_RELOAD_PORT = 35729;
var PROD_ENV = './build';
var PROD_ENV_PORT = 9000;

//static app data (json)
gulp.task('copy-data', function () {
    gulp.src([
        PROJECT_FILES + '/data/**/*'
    ])
        .pipe(gulp.dest(DEV_ENV + '/data'))
        .pipe(livereload(server));
});

// style -> css
gulp.task('stylus', function () {
    gulp.src([
        PROJECT_FILES + '/assets/**/*.styl',
        PROJECT_FILES + '/ui/**/*.styl'
    ])
        .pipe(stylus({
            use: ['nib']
        }))
        .on('error', console.log)
        .pipe(concat('jr-app.css'))
        .pipe(myth())
        .pipe(gulp.dest(DEV_ENV + '/css/'))
        .pipe(livereload(server));
});

// Jade -> HTML
gulp.task('jade', function () {
    //put index file to the root
    gulp.src([PROJECT_FILES + '/index.jade'])
        .pipe(jade({
            pretty: true
        }))
        .on('error', console.log)
        .pipe(gulp.dest(DEV_ENV))
        .pipe(livereload(server));

    //put all other files to templates
    gulp.src([
        PROJECT_FILES + '/ui/**/*.jade'
    ])
        .pipe(jade({
            pretty: true,
            data: {
                version: pkg.version
            }
        }))
        .on('error', console.log)
        .pipe(gulp.dest(DEV_ENV + '/templates'))
        .pipe(livereload(server));
});

// JS
gulp.task('js', function () {
    gulp.src([
        PROJECT_FILES + '/*.js',
        PROJECT_FILES + '/**/*.js'
    ])
        .pipe(concat('jr-app.js'))
        .pipe(gulp.dest(DEV_ENV + '/js'))
        .pipe(livereload(server));
});

// images
gulp.task('images', function () {
    gulp.src([
        PROJECT_FILES + '/assets/**/*.png',
        PROJECT_FILES + '/assets/**/*.jpg',
        PROJECT_FILES + '/assets/*.ico'
    ])
        .pipe(imagemin())
        .pipe(gulp.dest(DEV_ENV));
});

/**
 * run web server for dev code
 * 
 * @example
 * gulp http-server:dev
 */
gulp.task('http-server:dev', function () {
    connect()
        .use(require('connect-livereload')())
        .use(connect.static(DEV_ENV))
        .listen(DEV_ENV_PORT);

    console.log('Server listening on http://localhost:' + DEV_ENV_PORT);
});
/**
 * run web server for production code
 * 
 * @example
 * gulp http-server:prod
 */
gulp.task('http-server:prod', function () {
    connect()
        //.use(require('connect-livereload')())
        .use(connect.static(PROD_ENV))
        .listen(PROD_ENV_PORT);

    console.log('Server listening on http://localhost:%s', PROD_ENV_PORT);
});

/**
 * build dev version
 * 
 * @example
 * gulp build:dev
 */
gulp.task('build:dev', function() {
    gulp.start('env');
    buildDevApp();
});
/**
 * build UAT version
 * 
 * @example
 * gulp build:uat
 */
gulp.task('build:uat', function() {
    gulp.start('env:uat');
    buildDevApp();
});
/**
 * helper
 * prepare not compressed code with documentation
 */
function buildDevApp() {
    gulp.start('copy-data');
    gulp.start('stylus');
    gulp.start('jade');
    gulp.start('images');
    gulp.start('js');

    gulp.task('doc');
    gulp.start('add-doc-to-dev');
}

/**
 * prepare development version and run it
 * watch file changes
 */
gulp.task('serve:dev', function () {
    gulp.run('build:dev');
    addLivereloadToDev();
    gulp.run('http-server:dev');
});
/**
 * prepare UAT version and run it
 * watch file changes
 */
gulp.task('serve:uat', function () {
    gulp.run('build:uat');
    addLivereloadToDev();
    gulp.run('http-server:dev');
});
/**
 * helper
 * set livereload for development code
 */
function addLivereloadToDev() {
    server.listen(DEV_ENV_RELOAD_PORT, function (err) {
        if (err) return console.log(err);

        gulp.watch([
            PROJECT_FILES + '/**/*.styl'
        ], function () {
            gulp.run('stylus');
        });
        gulp.watch([
            PROJECT_FILES + '/**/*.jade'
        ], function () {
            gulp.run('jade');
        });
        gulp.watch([
            PROJECT_FILES + '/**/*.png',
            PROJECT_FILES + '/**/*.ico',
            PROJECT_FILES + '/**/*.jpg'
        ], function () {
            gulp.run('images');
        });
        gulp.watch([
            PROJECT_FILES + '/**/*.js'
        ], function () {
            gulp.run('js');
        });
    });
}

/**
 * default task is preapre dev version and run it
 * 
 * @example
 * gulp
 */
gulp.task('default', ['serve:dev']);

//TODO: not working
gulp.task('build:prod', function () {
    gulp.start('env:prod');

    //static data
    gulp.src([
        PROJECT_FILES + '/data/**/*'
    ])
        .pipe(gulp.dest(PROD_ENV + '/data'));

    // css
    gulp.src([
        PROJECT_FILES + '/assets/**/*.styl',
        PROJECT_FILES + '/ui/**/*.styl'
    ])
        .pipe(stylus({
            use: ['nib']
        }))
        .pipe(concat('jr-app.css'))
        .pipe(myth()) // add prefixes - http://www.myth.io/
        .pipe(csso()) // min css
        .pipe(gulp.dest(PROD_ENV + '/css/'));

    // jade
    gulp.src([
        PROJECT_FILES + '/index.jade'
    ])
        .pipe(jade())
        .pipe(gulp.dest(PROD_ENV));

    // js
    gulp.src([
        PROJECT_FILES + '/*.js',
        PROJECT_FILES + '/**/*.js'
    ])
        .pipe(concat('jr-app.js'))
        .pipe(ngmin({ dynamic: true }))
        .pipe(uglify())
        .pipe(gulp.dest(PROD_ENV + '/js'));

    gulp.src([
        PROJECT_FILES + '/ui/**/*.jade'
    ])
        .pipe(jade())
        .pipe(gulp.dest(PROD_ENV + '/templates'));

    // image
    gulp.src([
        PROJECT_FILES + '/assets/**/*.png',
        PROJECT_FILES + '/assets/**/*.jpg',
        PROJECT_FILES + '/assets/*.ico'
    ])
        .pipe(imagemin())
        .pipe(gulp.dest(PROD_ENV));
});
/**
 * prepare compressed production version and run it
 * 
 * @example
 * gulp serve:prod
 */
gulp.task('serve:prod', function () {
    gulp.run('build:prod');
    gulp.run('http-server:prod');
});

/**
 * publish app to Amazon S3 
 * --type=dev  - (default) publish from dev folder 
 * --type=prod - publish from build folder
 * 
 * @example
 * gulp publish
 * gulp publish --type=dev
 * gulp publish --type=prod
 */
gulp.task('publish', function () { 
    var publisher = awspublish.create({
        region: config.AWS.region,
        params: {
            Bucket: config.AWS.bucket
        },
        accessKeyId: config.AWS.key,
        secretAccessKey: config.AWS.secret
    });
    var input_path = (!argv.type || argv.type === 'dev') ? DEV_ENV : PROD_ENV;

    return gulp.src(input_path + '/**')
        .pipe(publisher.publish())
        .pipe(publisher.sync())
        .pipe(awspublish.reporter());
});

/**
 * Bump the version
 * --type=pre will bump the prerelease version *.*.*-x
 * --type=patch or no flag will bump the patch version *.*.x
 * --type=minor will bump the minor version *.x.*
 * --type=major will bump the major version x.*.*
 * --version=1.2.3 will bump to a specific version and ignore other flags
 * 
 * @example
 * gulp bump --type=patch
 */
gulp.task('bump', function() {
    var msg = 'Bumping versions';
    var type = argv.type;
    var version = argv.ver;

    var options = {
        key: "version"
    };
    if (version) {
        options.version = version;
        msg += ' to ' + version;
    } else {
        options.type = type;
        msg += ' for a ' + type;
    }

    console.log(msg);

    return gulp
        .src(config.bump_version.input_path)
        .pipe(bump(options))
        .pipe(gulp.dest('./'));
});

/**
 * Set environment variables (like mode, API endpoints, ...)
 * --type=prod
 * --type=staging
 * --type=dev (default)
 * can be run from command line with parameters
 * 
 * @example
 * gulp env
 * gulp env --type=dev
 * gulp env --type=uat
 * gulp env --type=staging
 * gulp env --type=prod
 */
gulp.task('env', function (done) {
    env(argv.type);
    done(); //callback (define that task completed, for syncronous task executing)
});
/**
 * separated tasks for setting appropriated environment variables
 * can be executed from gulp file
 * 
 * @example
 * gulp env:prod
 * gulp env:staging
 * gulp env:uat
 */
gulp.task('env:prod', function (done) {
    env('prod');
    done();
});
gulp.task('env:staging', function (done) {
    env('staging');
    done();
});
gulp.task('env:uat', function (done) {
    env('uat');
    done();
});
/**
 * set environment variables helper
 * @param {String} type - 'dev'|'prod'|'staging'
 */
function env(type) {
    var options = {
        'environment': (type || 'dev')
    };
    gulp.src(config.env.input_path)
        .pipe(gulpNgConfig(config.env.output_module_name, options))
        .pipe(gulp.dest(config.env.output_path));

}

/**
 * generate documentation for the app
 * 
 * @example
 * gulp doc
 */
gulp.task('doc', function () {
    var options = {
        scripts: [
            'http://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular.min.js',
            'http://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-animate.min.js',
            'https://cdnjs.cloudflare.com/ajax/libs/marked/0.3.5/marked.min.js',
            'https://cdnjs.cloudflare.com/ajax/libs/prettify/r298/prettify.min.js'
        ],
        html5Mode: false,
        startPage: '/api/jr-app.bl.searchService:JrSearch',
        title: "Jayride App documentation",
        titleLink: "/",
        image: "//d1lk4k9zl9klra.cloudfront.net/web/international/common/site/jayride-logo-120x70.png",
        imageLink: "/",
        bestMatch: false,
        loadDefaults: {
            angularAnimate: false,
            angular: false,
            marked: false,
            prettify: false
        }
    };

    return gulp.src(PROJECT_FILES + '/**/*.js')
        .pipe(gulpDocs.process(options))
        .pipe(gulp.dest(config.app_doc.output_path));
});

/**
 * just copy app documentation to development directory
 * production directory will not contain documentation
 * 
 * @example
 * gulp add-doc-to-dev
 */
gulp.task('add-doc-to-dev', function () {
    gulp.src([
            config.app_doc.output_path + '/**/*'
        ])
        .pipe(gulp.dest(DEV_ENV + '/doc'));
});

/**
 * prepare and run documentation
 * will host independent web server
 * 
 * @example
 * gulp serve:doc
 */
gulp.task('serve:doc', function () {
    gulp.start('doc');

    connect()
        .use(connect.static(config.app_doc.output_path))
        .listen(config.app_doc.doc_port);

    console.log('App doc is running on http://localhost:' + config.app_doc.doc_port);
});