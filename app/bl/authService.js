(function () {
    'use strict';

    /**
     * @ngdoc service
     * @name jr-app.bl.authService:JrAuth
     * @description 
     * this service allows to register/login/logout users.
     * it works with Auth0 
     */

    angular
        .module('jr-app.bl.authService', [])
        .factory('JrAuth', Service);

    /** @ngInject */
    function Service($q, AUTH0_CLIENT_ID, AUTH0_DOMAIN) {
        var AUTH0_CALLBACK_URL = location.href;

        var SESSION_KEY = 'id_token';

        var lock;
        var userProfile;

        init();

        /**
         * @ngdoc method
         * @name init
         * @methodOf jr-app.bl.authService:JrAuth
         * @description 
         * [private] initialize Auth0
         * @access private
         * @example
         * <pre>
         * JrAuth.init();
         * </pre>
         */
        function init() {
            lock = new Auth0Lock(
                AUTH0_CLIENT_ID,
                AUTH0_DOMAIN
            );
        }

        /**
         * @ngdoc method
         * @name login
         * @methodOf jr-app.bl.authService:JrAuth
         * @description 
         * show login/register Auth0 form
         * @returns {Object} promise
         * @example
         * <pre>
         * JrAuth.login();
         * </pre>
         */
        function login() {
            var deferred = $q.defer();

            var options = {
                mode: 'signin', //Set the show mode for the display. Allowed values are signin, signup and reset. Defaults to signin.
                focusInput: true, //If true, the focus is set to the email field on the widget. 
                rememberLastLogin: false, //Request for SSO data and enable Last time you signed in with[...] message. Defaults to true.
                popup: true, //If set to true, shows a popup when trying to login with a Social or Enterprise IdP. Defaults to true when a callback is set, otherwise false.
                //popupOptions: { width: 300, height: 400, left: 200, top: 300 },
                disableSignupAction: true,
                //signupLink: 'https://yoursite.com/signup',
                connections: ['Username-Password-Authentication'], // username and password signin form
                icon: 'http://res.jayride.com/Web/DotCom/Splash/jayride-logo.png', //'https://auth0.com/boot/badge.png',
                disableResetAction: true
                //resetLink: 'https://yoursite.com/reset-password',
                /*
                authParams: { //https://auth0.com/docs/libraries/lock/v10/sending-authentication-parameters
                    scope: 'openid'
                }, //https://auth0.com/docs/libraries/lock/customization#authparams-object-
                */
                //usernameStyle: 'username', //force `username` input style (email as default username style)
                //container: 'hiw-login-container', // render in `#hiw-login-container` element
                //socialBigButtons: false,
                //closable: false, // disable the closable action (close login popup)
                //loginAfterSignup: false, //Triggers a sign in call after sign up. Defaults to true.
                //integratedWindowsLogin: false, //Allows for Realm discovery by AD, LDAP connections. Defaults to true.
                //defaultUserPasswordConnection: 'test-database' //https://auth0.com/docs/libraries/lock/customization#defaultuserpasswordconnection-string-
                //defaultADUsernameFromEmailPrefix: false // https://auth0.com/docs/libraries/lock/customization#defaultadusernamefromemailprefix-boolean-
                //theme: 'mycroft' //`a0-theme-mycroft` | false //a0-theme-<theme-value>
                //callbackURL: 'http://mydomain.com/callback' // https://auth0.com/docs/libraries/lock/customization#callbackurl-string-
                //forceJSONP: true // Force JSONP requests for all auth0-js instance requests. This setup is useful when no CORS allowed. Defaults to false.
                //sso: true //Sets a cookie used for single sign on.
                //popupCallback: function(){}

                //The Auth0 footer icon is automatically removed from Lock for paying accounts. 
            };
            function onLogin(err, profile, id_token) {
                if (err) { // There was an error logging the user in
                    console.log(err);

                    deferred.reject(err);
                    return;
                }

                // User is logged in
                userProfile = profile;
                localStorage.setItem(SESSION_KEY, id_token);
                deferred.resolve(userProfile);
            }
            //show | showSignin | showSignup
            lock.show(options, onLogin);

            return deferred.promise;
        }

        /**
         * @ngdoc method
         * @name logout
         * @methodOf jr-app.bl.authService:JrAuth
         * @description 
         * clean user data
         * @returns {Object} promise
         * @example
         * <pre>
         * JrAuth.logout();
         * </pre>
         */
        function logout() {
            var deferred = $q.defer();

            localStorage.removeItem(SESSION_KEY);
            userProfile = null;
            deferred.resolve();
            
            return deferred.promise;
        }

        /**
         * @ngdoc method
         * @name isAuthenticated
         * @methodOf jr-app.bl.authService:JrAuth
         * @description 
         * check if user logged in
         * @returns {Boolean} true | false
         * @example
         * <pre>
         * JrAuth.isAuthenticated();
         * </pre>
         */
        function isAuthenticated() {
            return localStorage.getItem(SESSION_KEY) ? true : false;
        }

        /**
         * @ngdoc method
         * @name getToken
         * @methodOf jr-app.bl.authService:JrAuth
         * @description 
         * return authentication token
         * @returns {String|Null} token (if exists) or null
         * @example
         * <pre>
         * JrAuth.getToken();
         * </pre>
         */
        function getToken() {
            return localStorage.getItem(SESSION_KEY);
        }

         /**
         * @ngdoc method
         * @name getUser
         * @methodOf jr-app.bl.authService:JrAuth
         * @description 
         * return user profile data
         * @returns {Object} promise
         * @example
         * <pre>
         * JrAuth.getUser();
         * </pre>
         */
        function getUser() {
            var deferred = $q.defer();

            if(isAuthenticated()) {
                if(userProfile) {
                    deferred.resolve(userProfile);
                } else {
                    var id_token = localStorage.getItem(SESSION_KEY);
                    lock.getProfile(id_token, function (err, profile) {
                        if (err) {
                            deferred.reject(err);
                            return;
                        }
                        
                        userProfile = profile;
                        deferred.resolve(userProfile);
                    });
                }
            } else {
                deferred.reject(new Error('user is not logged in'));
            }

            return deferred.promise;
        }

        //public facade
        return {
            login: login,
            logout: logout,
            isAuthenticated: isAuthenticated,
            getUser: getUser,
            getToken: getToken
        };
    }
})();