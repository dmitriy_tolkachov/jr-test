(function () {
    'use strict';

    /**
     * @ngdoc service
     * @name jr-app.bl.searchService:JrSearch
     * @description this service allows to set auto-complete functionality
     */

    angular
        .module('jr-app.bl.searchService', [])
        .factory('JrSearch', Service);

    /** @ngInject */
    function Service($http, $q, AIRPORT_DATA_PATH) {

        var airportData = null; //full airports data
        var airportAutocompleteData = null; //auto-complete suggestions for airports

        /**
         * constructor that creates object for describing place
         * @param {string} location place`s name
         * @param {number} lat geo data - latitude
         * @param {number} lng geo data - latitude
         * @param {number} type 1 for google places, 2 for airports
         * @param {boolean} isAccurate
         * @constructor
         */
        var JrPlace = function(location, lat, lng, type, isAccurate) {
            this.location = location || null;
            this.lat = lat || null;
            this.lng = lng || null;
            this.type = type? +type : 1;
            this.isAccurate = (typeof(isAccurate) === "boolean") ? isAccurate : true;
        };
        JrPlace.prototype.toParamString = function(paramExtension) {
            paramExtension = paramExtension || '';

            return [
                paramExtension,
                'Location=',
                this.location,
                '&',
                paramExtension,
                'Lat=',
                this.lat,
                '&',
                paramExtension,
                'Lng=',
                this.lng,
                '&',
                paramExtension,
                'Type=',
                this.type,
                '&',
                paramExtension,
                'IsAccurate=',
                this.isAccurate
            ].join('');
        };

        /**
         * @ngdoc method
         * @name getAirportData
         * @private
         * @methodOf jr-app.bl.searchService:JrSearch
         * @description get all airports data (from cache or from server)
         * @param {boolean} force - if true load from server
         * @returns {Object} promise
         */
        function getAirportData(force) {
            var deferred = $q.defer();

            //TODO: cache data in localstorage

            if (force || !airportData) {
                $http.get(AIRPORT_DATA_PATH).then(function(res) {
                    airportData = res.data.airports;
                    deferred.resolve(airportData);
                }, function(res) {
                    airportData = null;
                    deferred.reject(res);
                });
            } else {
                deferred.resolve(airportData);
            }

            return deferred.promise;
        }

        /**
         * @ngdoc method
         * @name getAllAutoCompleteList
         * @private
         * @methodOf jr-app.bl.searchService:JrSearch
         * @description returns full array of strings for airport autocomplete
         * @returns {Object} promise
         */
        function getAllAutoCompleteList() {
            var deferred = $q.defer();

            getAirportData().then(function() {
                if (!airportAutocompleteData) {
                    var res = [];

                    airportData.forEach(function(airport) {
                        airport.terminals.forEach(function(terminal) {
                            //res.push(airport.name + ' (' + airport.iata +'), ' + terminal.name);
                            res.push(new JrPlace(airport.name + ' (' + airport.iata +'), ' + terminal.name, terminal.lat, terminal.lng, 2, true));
                        });
                    });

                    airportAutocompleteData = res;
                }

                deferred.resolve(airportAutocompleteData);
            });


            return deferred.promise;
        }

        /**
         * @ngdoc method
         * @name getAllAutoCompleteList
         * @methodOf jr-app.bl.searchService:JrSearch
         * @description set up places auto-complete for input text element
         * @param {string} inputElementId ui input element for suggestions
         * @param {function} onPlaceChanged callback when suggestion choosen
         * @example
         * <pre>
         * JrSearch.initPlacesAutocomplete('input-id', function(jrPlace) {console.log(jrPlace);});
         * </pre>
         */
        function initPlacesAutocomplete(inputElementId, onPlaceChanged) {
            var defaultBounds = new google.maps.LatLngBounds(
                new google.maps.LatLng(-33.8902, 151.1759),
                new google.maps.LatLng(-33.8474, 151.2631)
            ); //Sydney
            var input = document.getElementById(inputElementId); //HTTP DOM Element required for google auto-complete
            var options = {
                componentRestrictions: {country: 'au'},
                bounds: defaultBounds,
                types: [
                    'geocode'
                    //'establishment'
                ]
            };

            var autocomplete = new google.maps.places.Autocomplete(input, options);

            autocomplete.addListener('place_changed', function() {
                input.dispatchEvent(new Event('input')); //trigger event -> angular will know that input has been changed and will update model

                var place = autocomplete.getPlace();
                if (onPlaceChanged) {
                    onPlaceChanged(googlePlaceDataToJrPlaceData(place));
                }
            });

            /**
             * helper function to convert object
             * @param {object} place google place object
             * @returns {JrPlace}
             */
            function googlePlaceDataToJrPlaceData(place) {
                return new JrPlace(place.formatted_address, place.geometry.location.lat(), place.geometry.location.lng(), 1, true);
            }
        }

        /**
         * @ngdoc method
         * @name initAirportAutocomplete
         * @methodOf jr-app.bl.searchService:JrSearch
         * @description set up airports auto-complete for input text element
         * @param {string} inputElementId input element for airport data suggestions
         * @param {function} onPlaceChanged callback function
         * @example
         * <pre>
         * JrSearch.initAirportAutocomplete('input-id', function(jrPlace) {console.log(jrPlace);});
         * </pre>
         */
        function initAirportAutocomplete(inputElementId, onPlaceChanged) {
            getAllAutoCompleteList().then(function onDataReady(arrayOfAirportPlaces) {
                var options = {
                    hint: true,
                    highlight: true,
                    minLength: 1
                };
                var dataset = {
                    name: 'ds',
                    displayKey: 'location',
                    source: new Bloodhound({
                        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('location'), //property name in dataset
                        queryTokenizer: Bloodhound.tokenizers.whitespace,
                        local: arrayOfAirportPlaces
                    })
                };

                var input = document.getElementById(inputElementId);
                var $input = $(input); //jquery wrapper for setting up autocomplete plugin
                $input
                    .typeahead(options, dataset) //typeahead requers jquery obj
                    .on('typeahead:selected', function(obj, datum) {
                        input.dispatchEvent(new Event('input')); //trigger event -> angular will update input`s model (jquery .trigger does not work)
                        if(onPlaceChanged) {
                            onPlaceChanged(datum);
                        }
                    });
            });
        }

        //public facade
        return {
            initPlacesAutocomplete: initPlacesAutocomplete,
            initAirportAutocomplete: initAirportAutocomplete
        };
    }
})();