(function () {
    'use strict';

    /**
     * @ngdoc overview
     * @name jr-app.bl
     *
     * @description
     * business logic module
     * 
     * includes:
     * - authService
     * - mediatorService
     * - searchService
     * 
     * utilities:
     * - authInterceptor
     */

    angular
        .module('jr-app.bl', [
            'jr-app.bl.authInterceptor',
            'jr-app.bl.authService',
            'jr-app.bl.mediatorService',
            'jr-app.bl.searchService'
        ]);

})();