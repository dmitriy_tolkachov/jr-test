(function () {
    'use strict';

    /**
     * @ngdoc service
     * @name jr-app.bl.mediatorService:JrMediator
     * @description allows to different components to communicate with each other via events
     */

    angular
        .module('jr-app.bl.mediatorService', [])
        .factory('JrMediator', Service);

    /** @ngInject */
    function Service($rootScope) {

        /**
         * array of SubscriptionClass
         * @type {Array}
         */
        var subscriptions = [];
        /**
         * subscription entity
         * @param {String} event
         * @param {Function} callback
         * @param {Function} unsubscribe
         * @param {Object=} scope - scope for witch event subscription occurred
         * @constructor
         */
        var SubscriptionClass = function(event, callback, unsubscribe, scope) {
            this.event = event;
            this.callback = callback;
            this.unsubscribe = unsubscribe;
            this.scope = scope;
        };
        /**
         * @ngdoc method
         * @name getSubscription
         * @methodOf jr-app.bl.mediatorService:JrMediator
         * @description
         * allows to check if subscription exists
         * @param {String} evtName - name of event
         * @param {Function} callback - function that should be triggered on event
         * @param {Object=} scope - angular scope
         * @returns {Object} promise
         * @private
         */
        function getSubscription(evtName, callback, scope) {
            return subscriptions.find(function(subscription) {
                return subscription.scope === scope && subscription.event === evtName && subscription.callback.toString() === callback.toString();
                /*if (scope) {
                return subscription.scope === scope && subscription.event === evtName && subscription.callback.toString() === callback.toString();
                } else {
                return subscription.event === evtName && subscription.callback.toString() === callback.toString();
                }*/
            });
        }

        /**
         * @ngdoc method
         * @name subscribe
         * @methodOf jr-app.bl.mediatorService:JrMediator
         * @description
         * subscribe on global event
         * @param {String} evtName - name of event
         * @param {Function} callback - function that should be triggered on event
         * @param {Object=} scope - component|controller scope for unsubscribe event
         */
        function subscribe(evtName, callback, scope) {
            //avoid duplicated subscriptions
            if (getSubscription(evtName, callback, scope)) { //duplicated subscription
                return new Error('such subscription already exists.');
            } else { //unique subscription
                var unsubscribeFn =  $rootScope.$on(evtName, callback);
                subscriptions.push(new SubscriptionClass(evtName, callback, unsubscribeFn, scope));

                //if scope defined for event subscription -> unsubscribe event on scope destroy
                if (scope && scope.$on) {
                    scope.$on('$destroy', function() {
                        unsubscribe(evtName, callback, scope);
                    });
                }

                return unsubscribeFn;
            }
        }
        /**
         * @ngdoc method
         * @name unsubscribe
         * @methodOf jr-app.bl.mediatorService:JrMediator
         * @description
         * unsubscribe from listening event
         * @param {String} evtName - name of event
         * @param {Function} callback - function that should be triggered on event
         * @param {Object=} scope - angular scope object
         */
        function unsubscribe(evtName, callback, scope) {
            var subscription = getSubscription(evtName, callback, scope);
            if (subscription) {
                var subscriptionIndex = subscriptions.indexOf(subscription);
                subscription.unsubscribe(); //unsubscribe event
                subscriptions.splice(subscriptionIndex,1); //remove subscription from the list
                return null;
            } else {
                return new Error('no such subscription "%s", %o', evtName, callback);
            }
        }
        /**
         * @ngdoc method
         * @name trigger
         * @methodOf jr-app.bl.mediatorService:JrMediator
         * @description
         * fire global event
         * @param {String} evtName  - name of event
         * @param {Object|array} data - data that will passed within event
         */
        function trigger(evtName, data) {
            $rootScope.$emit(evtName, data);
        }



        window.JrMediatorSubscriptions = subscriptions; //TODO: for testing/debugging purposes only

        //public facade
        return {
            on: subscribe,
            off: unsubscribe,
            trigger: trigger
        };
    }
})();