(function() {
'use strict';

    /**
     * @ngdoc service
     * @name jr-app.bl.authInterceptor:JrAuthInterceptor
     * @description 
     * 
     */

    angular
        .module('jr-app.bl.authInterceptor', [])
        .factory('JrAuthInterceptor', Service);

    Service.$inject = ['JrAuth'];
    
    function Service(JrAuth) {
        /**
         * @ngdoc method
         * @name login
         * @methodOf jr-app.bl.authInterceptor:JrAuthInterceptor
         * @description 
         * set authentication token to header for each http request
         * @param {Object} config - request configuration
         * @returns {Object} config (updated or the same)
         * @example
         * <pre>
         * JrAuthInterceptor.request();
         * </pre>
         */
        function request(config) { 
            if (JrAuth.isAuthenticated()) {
                config.headers.Authorization = 'Bearer ' + JrAuth.getToken();
            }

            return config;
        }

         /**
         * @ngdoc method
         * @name login
         * @methodOf jr-app.bl.authInterceptor:JrAuthInterceptor
         * @description 
         * update server response
         * @param {Object} response - response from an API
         * @returns {Object} response (updated or the same)
         * @example
         * <pre>
         * JrAuthInterceptor.response();
         * </pre>
         */
        function response(response) { 
            return response;
        }

        //public facade
        return {
            request: request,
            response: response
        };
    }
})();