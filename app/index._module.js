(function () {
    'use strict';

    /**
     * @ngdoc overview
     * @name jr-app
     *
     * @description
     * app module
     *
     */

    angular
        .module('jr-app', [
            'ui.router',

            'jr-app.ui',
            'jr-app.bl',

            'jr.config'
        ]);

})();