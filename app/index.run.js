(function () {
    'use strict';

    /**
     * @ngdoc overview
     * @name jr-app
     *
     * @description
     * app start
     *
     */

    angular
        .module('jr-app')
        .run(indexRun);

    //on app run
    function indexRun() {
    }

})();