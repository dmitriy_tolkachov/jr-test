(function () {
    'use strict';

    /**
     * @ngdoc overview
     * @name jr-app.ui
     *
     * @description
     * presentation module
     * 
     * containes ui components:
     * - jr-footer
     * - jr-form
     * - jr-header
     * - jr-login
     * - jr-suppliers
     * - jr-trust
     * - jr-usp
     * - jr-user-menu
     *  
     * and pages:
     * - master-page
     * - home page
     * - login page
     * 
     */

    angular
        .module('jr-app.ui', [
            'jr-app.ui.jr-footer',
            'jr-app.ui.jr-form',
            'jr-app.ui.jr-header',
            'jr-app.ui.jr-login',
            'jr-app.ui.jr-suppliers',
            'jr-app.ui.jr-trust',
            'jr-app.ui.jr-usp',
            'jr-app.ui.jr-user-menu'
        ]);

})();
