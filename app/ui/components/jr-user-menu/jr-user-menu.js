(function () {
    'use strict';

    /**
     * @ngdoc directive
     * @name jr-app.ui.jr-user-menu:jrUserMenu
     * @restrict E
     * @scope
     * @description inserts user menu (visible if user authenticated)
     * @Example
     * <jr-user-menu/>
     */

    angular
        .module('jr-app.ui.jr-user-menu', [])
        .directive('jrUserMenu', customDirective);

    /** @ngInject */
    function customDirective() {
        var directive = {
            restrict: 'E',
            templateUrl: 'templates/components/jr-user-menu/jr-user-menu.html',
            scope: {},
            controller: customController,
            controllerAs: 'vm'
        };

        return directive;

        /** @ngInject */
        function customController($state, JrAuth, JrMediator){
            var _this = this;

            //properties
            this.isAuthenticated = false;
            this.userProfile;

            //methods
            this.logout = function () {
                JrAuth.logout().then(function () {
                    _this.isAuthenticated = false;

                    JrMediator.trigger('user:logout:success');
                });
            };

            function init() {
                _this.isAuthenticated = JrAuth.isAuthenticated(); //init value
                JrAuth.getUser().then(function(profile) {
                    _this.userProfile = profile;
                });
            }

            //events
            JrMediator.on('user:login:success', init);
            JrMediator.on('user:logout:success', init);

            //init
            init();
        }
    }

})();
