(function () {
    'use strict';

    /**
     * @ngdoc directive
     * @name jr-app.ui.jr-footer:jrFooter
     * @restrict E
     * @scope
     * @description inserts footer ui component
     * @Example
     * <jr-footer/>
     */

    angular
        .module('jr-app.ui.jr-footer', [])
        .directive('jrFooter', customDirective);

    /** @ngInject */
    function customDirective() {
        var directive = {
            restrict: 'E',
            templateUrl: 'templates/components/jr-footer/jr-footer.html',
            scope: {},
            controller: customController,
            controllerAs: 'vm'
        };

        return directive;

        /** @ngInject */
        function customController(env){
            this.env = env.charAt(0).toUpperCase();

            var LinkItem = function(label, link) {
                this.label = label || '';
                this.link = link || '';
            };

            this.footerData = {
                transferServices: {
                    title: 'Airport Transfer Services',
                    list: [
                        new LinkItem('1', 'http://au.jayride.com/Airport-transfer-services/12-1'),
                        new LinkItem('2', 'http://au.jayride.com/Airport-transfer-services/12-2'),
                        new LinkItem('3', 'http://au.jayride.com/Airport-transfer-services/12-3'),
                        new LinkItem('4', 'http://au.jayride.com/Airport-transfer-services/12-4'),
                        new LinkItem('5', 'http://au.jayride.com/Airport-transfer-services/12-5'),
                        new LinkItem('6', 'http://au.jayride.com/Airport-transfer-services/12-6'),
                        new LinkItem('7', 'http://au.jayride.com/Airport-transfer-services/12-7'),
                        new LinkItem('8', 'http://au.jayride.com/Airport-transfer-services/12-8'),
                        new LinkItem('9', 'http://au.jayride.com/Airport-transfer-services/12-9')
                    ]
                },
                transferConnections: {
                    title: 'Airport Transfer Connections',
                    list: [
                        new LinkItem('A', 'http://au.jayride.com/Airport-connections-names-starting-with-a/10-a'),
                        new LinkItem('B', 'http://au.jayride.com/Airport-connections-names-starting-with-a/10-b'),
                        new LinkItem('C', 'http://au.jayride.com/Airport-connections-names-starting-with-a/10-c'),
                        new LinkItem('D', 'http://au.jayride.com/Airport-connections-names-starting-with-a/10-d'),
                        new LinkItem('E', 'http://au.jayride.com/Airport-connections-names-starting-with-a/10-e'),
                        new LinkItem('F', 'http://au.jayride.com/Airport-connections-names-starting-with-a/10-f'),
                        new LinkItem('G', 'http://au.jayride.com/Airport-connections-names-starting-with-a/10-g'),
                        new LinkItem('H', 'http://au.jayride.com/Airport-connections-names-starting-with-a/10-h'),
                        new LinkItem('I', 'http://au.jayride.com/Airport-connections-names-starting-with-a/10-i'),
                        new LinkItem('J', 'http://au.jayride.com/Airport-connections-names-starting-with-a/10-j'),
                        new LinkItem('K', 'http://au.jayride.com/Airport-connections-names-starting-with-a/10-k'),
                        new LinkItem('L', 'http://au.jayride.com/Airport-connections-names-starting-with-a/10-l'),
                        new LinkItem('M', 'http://au.jayride.com/Airport-connections-names-starting-with-a/10-m'),
                        new LinkItem('N', 'http://au.jayride.com/Airport-connections-names-starting-with-a/10-n'),
                        new LinkItem('O', 'http://au.jayride.com/Airport-connections-names-starting-with-a/10-o'),
                        new LinkItem('P', 'http://au.jayride.com/Airport-connections-names-starting-with-a/10-p'),
                        new LinkItem('Q', 'http://au.jayride.com/Airport-connections-names-starting-with-a/10-q'),
                        new LinkItem('R', 'http://au.jayride.com/Airport-connections-names-starting-with-a/10-r'),
                        new LinkItem('S', 'http://au.jayride.com/Airport-connections-names-starting-with-a/10-s'),
                        new LinkItem('T', 'http://au.jayride.com/Airport-connections-names-starting-with-a/10-t'),
                        new LinkItem('U', 'http://au.jayride.com/Airport-connections-names-starting-with-a/10-u'),
                        new LinkItem('V', 'http://au.jayride.com/Airport-connections-names-starting-with-a/10-v'),
                        new LinkItem('W', 'http://au.jayride.com/Airport-connections-names-starting-with-a/10-w'),
                        new LinkItem('X', 'http://au.jayride.com/Airport-connections-names-starting-with-a/10-x'),
                        new LinkItem('Y', 'http://au.jayride.com/Airport-connections-names-starting-with-a/10-y'),
                        new LinkItem('Z', 'http://au.jayride.com/Airport-connections-names-starting-with-a/10-z'),
                    ]
                },
                links: [
                    {
                        title: 'Our Company',
                        items: [
                            new LinkItem('About Us', 'http://au.jayride.com/Misc/AboutUs'),
                            new LinkItem('Help Center', 'http://au.jayride.com/Misc/Help'),
                            new LinkItem('Contact Support', 'http://au.jayride.com/Misc/Support'),
                        ]
                    },
                    {
                        title: 'Terms of Use',
                        items: [
                            new LinkItem('Terms & Conditions', 'http://au.jayride.com/Misc/TermsAndConditions'),
                            new LinkItem('Privacy Policy', 'http://au.jayride.com/Misc/Privacy'),
                            new LinkItem('Refunds & Cancellation', 'http://au.jayride.com/Misc/RefundsAndCancellation'),
                            new LinkItem('Cookies', 'http://au.jayride.com/Misc/Cookies')
                        ]
                    },
                    {
                        title: 'Work with Us',
                        items: [
                            new LinkItem('Transport Suppliers', 'http://au.jayride.com/Misc/TransportSupplierPartners'),
                            new LinkItem('Travel Agents', 'http://au.jayride.com/Misc/TravelAgentPartners'),
                            new LinkItem('Partner Affiliates', 'http://au.jayride.com/Misc/AffiliatePartners'),
                            new LinkItem('Agent Login', 'https://agent.jayride.com/Login/Agent')
                        ]
                    }
                ]
            };
        }
    }

})();
