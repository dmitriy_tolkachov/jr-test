(function () {
    'use strict';

    /**
     * @ngdoc directive
     * @name jr-app.ui.jr-suppliers:jrSuppliers
     * @restrict E
     * @scope
     * @description inserts Jayride partners info
     * @Example
     * <jr-suppliers/>
     */

    angular
        .module('jr-app.ui.jr-suppliers', [])
        .directive('jrSuppliers', customDirective);

    /** @ngInject */
    function customDirective() {
        var directive = {
            restrict: 'E',
            templateUrl: 'templates/components/jr-suppliers/jr-suppliers.html',
            scope: {},
            controller: customController,
            controllerAs: 'vm'
        };

        return directive;

        /** @ngInject */
        function customController(){
            //TODO: load data from API (or json)

            var SupplierDataItem = function(iconClass) {
                this.iconClass = iconClass || ''; //default value here
            };

            this.suppliersData = {
                title: 'BOOK YOUR AIRPORT TRANSFER WITH GREAT TRANSPORT SUPPLIERS LIKE THESE...',
                suppliers: [
                    new SupplierDataItem('club-class'),
                    new SupplierDataItem('airport-king'),
                    new SupplierDataItem('air-bus'),
                    new SupplierDataItem('heathrow-shuttle'),
                    new SupplierDataItem('bath'),
                    new SupplierDataItem('active-response'),
                    new SupplierDataItem('taxi'),
                    new SupplierDataItem('saville-exclusive-cars'),
                    new SupplierDataItem('conxion'),
                    new SupplierDataItem('airport-specialists-cars'),
                    new SupplierDataItem('etm-cars'),
                    new SupplierDataItem('super-shuttle'),
                    new SupplierDataItem('newmarket-airport-taxis'),
                    new SupplierDataItem('capital-cars'),
                ]
            };

        }
    }

})();
