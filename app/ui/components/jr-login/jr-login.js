(function () {
    'use strict';

    /**
     * @ngdoc directive
     * @name jr-app.ui.jr-login:jrLogin
     * @restrict E
     * @scope
     * @description inserts Jayride login form
     * @Example
     * <jr-login/>
     */

    angular
        .module('jr-app.ui.jr-login', [])
        .directive('jrLogin', customDirective);

    /** @ngInject */
    function customDirective() {
        var directive = {
            restrict: 'E',
            templateUrl: 'templates/components/jr-login/jr-login.html',
            scope: {},
            controller: customController,
            controllerAs: 'vm'
        };

        return directive;

        /** @ngInject */
        function customController(JrAuth, JrMediator, $http){
            var _this = this;

            this.isAuthenticated = JrAuth.isAuthenticated(); //init value

            this.showLogin = function() {
                JrAuth.login().then(function (profile) {
                    _this.isAuthenticated = true;

                    JrMediator.trigger('user:login:success', profile);
                });
            };

            this.logout = function () {
                JrAuth.logout().then(function () {
                    _this.isAuthenticated = false;

                    JrMediator.trigger('user:logout:success');
                });
            };

            //TODO: for testing auth requests
            this.testRequest = function () {
                $http.get('/api/test').then(
                    function onOk(response) {
                    
                    }, 
                    function onError(err) {
                        
                    });
            };
        }
    }

})();
