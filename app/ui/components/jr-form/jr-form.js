(function () {
    'use strict';

    /**
     * @ngdoc directive
     * @name jr-app.ui.jr-form:jrForm
     * @restrict E
     * @scope
     * @description inserts client request form
     * @Example
     * <jr-form/>
     */

    angular
        .module('jr-app.ui.jr-form', [])
        .directive('jrForm', customDirective);

    /** @ngInject */
    function customDirective() {
        var directive = {
            restrict: 'E',
            templateUrl: 'templates/components/jr-form/jr-form.html',
            scope: {},
            controller: customController,
            controllerAs: 'vm'
        };

        return directive;

        /** @ngInject */
        function customController($timeout, JrSearch){

            var TabItem = function(id, label, isActive) {
                this.id = id || Math.random();
                this.label = label || '';
                this.isActive = !!isActive || false;
            };

            var FROM_AIRPORT_INPUT_ID = 'from-airport';
            var TO_AIRPORT_INPUT_ID = 'to-airport';
            var FROM_PLACE_INPUT_ID = 'from-place';
            var TO_PLACE_INPUT_ID = 'to-place';

            this.formModel = {
                fromAirport: {
                    from: '', //visible value
                    fromData: null, //metadata
                    to: '', //visible value
                    toData: null //metadata
                },
                toAirport: {
                    from: '', //visible value
                    fromData: null, //metadata
                    to: '', //visible value
                    toData: null //metadata
                }
            };
            this.currentTab = null;
            //TODO: this data can be loaded from API
            this.formData = {
                title: 'Book a reliable transfer at the best price!',
                submitLabel: 'FIND TRANSFER',
                labels: {
                    fromAirport: 'From Airport Terminal',
                    toAirport: 'To Airport Terminal',
                    airportPlaceholder: 'e.g. Sydney Airport (SYD), T1 International Terminal',
                    toPoint: 'To Address',
                    fromPoint: 'From Address',
                    pointPlaceholder: 'e.g. 46 Church Street, Parramatta NSW',
                    pointTip: 'Please enter accurate location (e.g. street address, hotel name etc.)',
                    airportTip: 'Please enter airport name and select from the list'
                },
                tabs: [
                    new TabItem('fromAirport', 'Pick Up From Airport', true),
                    new TabItem('toAirport', 'Drop-Off To Airport', false)
                ]
            };

            /**
             * on select tab
             * @param {object} tabItem
             */
            this.chooseTab = function(tabItem) {
                this.formData.tabs.forEach(function(item) {
                    item.isActive = (tabItem.id === item.id) ? true : false;
                });
                this.currentTab = tabItem;
            };

            /**
             * on submit form
             */
            this.submitForm = function() {
                var requestData = this.formModel[this.currentTab.id];
                console.log('%o', requestData);

                //this is just for demonstration
                var link = [
                    'http://au.jayride.com/Find',
                    '?',
                    requestData.fromData.toParamString('From'),
                    '&',
                    requestData.toData.toParamString('To')
                ].join('');

                window.location.href = link;
            };

            //helper method
            function setPlaceMetaData(placeMetadata) {
                this.formModel[this.currentTab.id][(this.currentTab.id == 'fromAirport') ? 'toData' : 'fromData' ] = placeMetadata;
            }
            //helper methods
            function setAirportMetaData(airportMetadata) {
                this.formModel[this.currentTab.id][(this.currentTab.id == 'fromAirport') ? 'fromData' : 'toData' ] = airportMetadata;
            }

            /**
             * controller initialization method
             */
            function init() {
                //set current tab from formData
                this.currentTab = this.formData.tabs.find(function(item){return item.isActive;});

                var setPlaceMetaDataBinded = setPlaceMetaData.bind(this),
                    setAirportMetaDataBinded = setAirportMetaData.bind(this);

                //angular hack - will be executed on render page
                $timeout(function() {
                    JrSearch.initAirportAutocomplete(FROM_AIRPORT_INPUT_ID, setAirportMetaDataBinded);
                    JrSearch.initPlacesAutocomplete(TO_PLACE_INPUT_ID, setPlaceMetaDataBinded);

                    JrSearch.initPlacesAutocomplete(FROM_PLACE_INPUT_ID, setPlaceMetaDataBinded);
                    JrSearch.initAirportAutocomplete(TO_AIRPORT_INPUT_ID, setAirportMetaDataBinded);
                }, 0);
            }

            //component initialization
            init.apply(this);
        }
    }

})();
