(function () {
    'use strict';

    /**
     * @ngdoc directive
     * @name jr-app.ui.jr-header:jrHeader
     * @restrict E
     * @scope
     * @description inserts site header
     * @Example
     * <jr-header/>
     */

    angular
        .module('jr-app.ui.jr-header', [])
        .directive('jrHeader', customDirective);

    /** @ngInject */
    function customDirective() {
        var directive = {
            restrict: 'E',
            templateUrl: 'templates/components/jr-header/jr-header.html',
            scope: {},
            controller: customController,
            controllerAs: 'vm'
        };

        return directive;

        /** @ngInject */
        function customController(){
            this.headerData = {
                title: 'Jayride Australia',
                slogan: 'The best airport shuttle prices, guaranteed!',
                help: {
                    label: 'Get Help',
                    iconClass: 'icon-envelop',
                    link: 'Contact Support'
                },
                domains: [
                    {
                        iconClass: 'flag-au',
                        label: 'Australia'
                    },
                    {
                        iconClass: 'flag-ie',
                        label: 'Ireland'
                    },
                    {
                        iconClass: 'flag-nz',
                        label: 'New Zealand'
                    },
                    {
                        iconClass: 'flag-uk',
                        label: 'United Kingdom'
                    },
                    {
                        iconClass: 'flag-us',
                        label: 'United States'
                    }
                ]
            };

            this.currentDomain = this.headerData.domains[0];

            this.chooseDomain = function(domain){
                this.currentDomain = domain;
            };
        }
    }

})();
