(function () {
    'use strict';

    /**
     * @ngdoc directive
     * @name jr-app.ui.jr-usp:jrUsp
     * @restrict E
     * @scope
     * @param {string} type depending on type component data and view will be different
     * @description inserts Jayride unique selling proposals info
     * @Example
     * <jr-usp/>
     * <jr-usp type="footer"/>
     */

    angular
        .module('jr-app.ui.jr-usp', [])
        .directive('jrUsp', customDirective);

    /** @ngInject */
    function customDirective() {
        var directive = {
            restrict: 'E',
            templateUrl: 'templates/components/jr-usp/jr-usp.html',
            scope: {
                type: '@'
            },
            controller: customController,
            controllerAs: 'vm'
        };

        return directive;

        /** @ngInject */
        function customController($scope){
            //TODO: load this data from API

            var UspDataItem = function(iconClass, title, description) {
                this.title = title || '';
                this.description = description || '';
                this.iconClass = iconClass || '';
            };

            switch ($scope.type) {
                case 'footer':
                    this.uspData = [
                        new UspDataItem('refund-guarantee', null, '100% REFUND GUARANTEE'),
                        new UspDataItem('price-guarantee', null, 'BEST PRICE GUARANTEE'),
                        new UspDataItem('instant-confirmation', null, 'INSTANT CONFIRMATION')
                    ];
                    break;
                default:
                    this.uspData = [
                        new UspDataItem('reliable-transport', 'RELIABLE TRANSPORT', 'Find reliable transport, at the best price.'),
                        new UspDataItem('every-destination', 'EVERY DESTINATION', 'Get to your destination, anywhere in Australia.'),
                        new UspDataItem('book-instantly', 'BOOK INSTANTLY', 'Book your transport online with instant confirmation.')
                    ];
            }
        }
    }

})();
