(function () {
    'use strict';

    /**
     * @ngdoc directive
     * @name jr-app.ui.jr-trust:jrTrust
     * @restrict E
     * @scope
     * @description inserts Jayride testimonials and key reasons to use service info
     * @Example
     * <jr-trust/>
     */

    angular
        .module('jr-app.ui.jr-trust', [])
        .directive('jrTrust', customDirective);

    /** @ngInject */
    function customDirective() {
        var directive = {
            restrict: 'E',
            templateUrl: 'templates/components/jr-trust/jr-trust.html',
            scope: {},
            controller: customController,
            controllerAs: 'vm'
        };

        return directive;

        /** @ngInject */
        function customController(){
            var TestimonialItem = function(text, author) {
                this.text = text || '';
                this.author = author || '';
            };

            this.trustData = {
                testimonialsTitle: 'WHAT PEOPLE SAY ABOUT JAYRIDE',
                testimonials: [
                    new TestimonialItem('Fantastic service, friendly driver, on time. Very happy. Would recommend to my friends.', 'Natasha, Jayride passenger'),
                    new TestimonialItem('The booking system was easy and straightforward. The service was excellent. They were on time, great price, and no hassles. Thanks heaps on a job well done!', 'Freddy, Jayride passenger')
                ],
                bookWithUsTitle: '8 Reasons to book with us',
                bookWithUsReasons: [
                    '100% Refund Guarantee',
                    'Best Price Guarantee',
                    'Pre-Pay Securely Online',
                    'Bookings Instantly Confirmed',
                    'Reliable Transport Providers',
                    'Compare Multiple Quotes',
                    'Every Destination in Australia',
                    'Door to Door, Day and Night'
                ],
                bookingWithUsSummary: 'OVER 15,000 PASSENGERS BOOKED!'
            };
        }
    }

})();
