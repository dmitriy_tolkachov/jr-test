(function () {
    'use strict';

    /**
     * @ngdoc overview
     * @name jr-app
     *
     * @description
     * app router
     *
     */

    angular
        .module('jr-app')
        .config(routerConfig);

    function routerConfig($locationProvider, $stateProvider, $urlRouterProvider) {
        //$locationProvider.html5Mode(true);

        $stateProvider
            .state('app', {
                //abstract: true,
                templateUrl: 'templates/master-page.html'
            })
            .state('app.home', {
                url: '/',
                templateUrl: 'templates/pages/home/home.html'
            })
            .state('app.login', {
                url: '/login',
                templateUrl: 'templates/pages/auth/login.html'
            })
        ;

        $urlRouterProvider.otherwise('/');
    }

})();