(function () {
    'use strict';

    /**
     * @ngdoc overview
     * @name jr-app
     *
     * @description
     * app configuration
     *
     */

    angular
        .module('jr-app')
        .config(appConfig);

    function appConfig($httpProvider) {
        $httpProvider.interceptors.push('JrAuthInterceptor');
    }

})();