# How to set up

`sudo npm install -g`

`npm install`

`npm start` (or `gulp`) - build dev environment and run dev server (http://localhost:9000/)

`npm run clean` - remove dev environment

(not completed) `npm serve:prod` | `gulp serve:prod` - for production build

# Demo & Versions

* v0.0.5 added auth interceptor - add authentication token to a http header on each request to API 
* v0.0.4 added app documentaion (gulp serve:doc)
* v0.0.3 added environment variables and version bumping
* v0.0.2 [jayride-test.tk.s3-website.eu-central-1.amazonaws.com](http://jayride-test.tk.s3-website.eu-central-1.amazonaws.com/#/)
* v0.0.1 https://dl.dropboxusercontent.com/u/17759370/jayride/index.html

# Atchitecture & Description

![jayride_v0.2.png](https://bitbucket.org/repo/zkBpg7/images/3215671440-jayride_v0.2.png)

* **package.json** - contains project description, commands that can be executed, list of dependencies, dev environment settings
* **environment.json** - contains description of all environments. All configuration variables should be placed here. Appropriated part of this file will be used in application.
* **gulpfile.js** - contains gulp tasks for build, running and publishing application

* **app** folder - contains application source code (raw code, can`t be hosted)
* **dev** folder - contains development or UAT ready application (can be hosted) 
* **build** folder - contains staging or production ready application (can be hosted)
* **doc** folder - contains separated generated app documentaion

_module.js - files that group angular modules to some global modules-namespaces. Strarts from _ because should be added before other files.